<?php

ini_set('zlib.output_compression', 'On');
ini_set('zlib.compression_level', 9);
ob_start();

$filename = 'comments.sqlite';
$dbh = new SQLite3($filename);

$create_stmts = array(
	'PRAGMA foreign_keys=OFF',
	'BEGIN TRANSACTION',
	<<<'EOF'
	CREATE TABLE IF NOT EXISTS "comments" (
		"id" 		INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
		"author"	TEXT	NOT NULL
					DEFAULT 'Anonymous',
		"comment"	TEXT	NOT NULL,
		"date"		INTEGER NOT NULL
					DEFAULT (
						CAST (
							strftime('%s', 'now')
							AS INTEGER
						)
					)
	);
EOF,
	'COMMIT'
);

foreach ($create_stmts as $stmt) {
	if (!$dbh->exec($stmt)) {
		die("Failed to execute $stmt");
	}
}
