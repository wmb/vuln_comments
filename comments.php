<?php

require_once 'db.php';

$get_comments_stmt = <<<'EOF'
	SELECT		"id",
			"author",
			"comment",
			datetime("date", 'unixepoch') AS "time"
	FROM		"comments"
	ORDER BY	"comment.date"	ASC
EOF;

$comments = $dbh->query($get_comments_stmt);

header('Content-Type: application/json; charset=UTF-8');
header('DNT: 1');
header('X-Powered-By: Nothing', TRUE);

function vulnescape($string) {
	$pattern = "@<(?<endtag>/?)(?<tag>script)(?<stuff>[^>]*)>@mXsu";
	return preg_replace($pattern, '', $string);
}

$results = array();
$colnames = array();

$numcols = $comments->numColumns();

for ($i = 0; $i < $numcols; $i++) {
	$colnames[] = $comments->columnName($i);
}

while ($row = $comments->fetchArray(SQLITE3_NUM)) {
	$result = array();
	for ($i = 0; $i < $numcols; $i++) {
		$text = vulnescape($row[$i]);
		$result[$colnames[$i]] = $text;
	}
	$results[] = $result;
}

$dbh->close();

echo json_encode($results);

ob_end_flush();
