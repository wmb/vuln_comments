window.addEventListener('load', e => {

	const verif = {
		"num1": undefined,
		"num2": undefined
	};

	function human_verif() {
		const get_random_int = (min, max) => {
			min = Math.ceil(min);
			max = Math.floor(max);
			return Math.floor(Math.random() * (max - min)) + min;
		};
		const label = document.getElementById("veriflabel");
		const num1 = get_random_int(1, 21);
		const num2 = get_random_int(1, 21);
		label.textContent = `${num1} + ${num2} = `;
		verif['num1'] = num1;
		verif['num2'] = num2;
	}

	function update_comments() {

		function add_row(tbody, comment) {
			const tr = document.createElement('tr');
			const id_td = document.createElement('td');
			const au_td = document.createElement('td');
			const cm_td = document.createElement('td');
			const tm_td = document.createElement('td');

			id_td.textContent = comment['id'];
			au_td.textContent = comment['author'];
			cm_td.innerHTML   = comment['comment'];
			tm_td.textContent = comment['time'];

			[id_td, au_td, cm_td, tm_td].forEach(td => {
				tr.appendChild(td);
			});

			tbody.appendChild(tr);

			const scripts = cm_td.getElementsByTagName("script");
			for (let i = 0; i < scripts.length; ++i) {
				console.log("Evaluating script");
				console.log(scripts.item(i));
				//eval(scripts.item(i).innerHTML);
				const script = scripts.item(i);
				const s = document.createElement('script');
				if (script.hasAttribute('src'))
					s.setAttribute('src', script.getAttribute('src'));
				s.innerHTML = script.innerHTML;
				document.head.appendChild(s);
			}
		}

		function setup_table() {
			const table = document.createElement('table');
			const thead = document.createElement('thead');
			const tr    = document.createElement('tr');
			const id_th = document.createElement('th');
			const au_th = document.createElement('th');
			const cm_th = document.createElement('th');
			const tm_th = document.createElement('th');

			id_th.textContent = "id";
			au_th.textContent = "author";
			cm_th.textContent = "comment";
			tm_th.textContent = "time";

			[id_th, au_th, cm_th, tm_th].forEach(th => {
				tr.appendChild(th);
			});

			thead.appendChild(tr);
			table.appendChild(thead);

			return table;
		}

		const div = document.getElementById("comments");
		[].slice.call(div.children)
			.forEach(child => div.removeChild(child));

		const table = setup_table();
		div.appendChild(table);

		const tbody = document.createElement('tbody');
		table.appendChild(tbody);

		const xhr = new XMLHttpRequest();
		xhr.addEventListener("load", () => {
			const comments = JSON.parse(xhr.responseText);
			comments.forEach(comment => add_row(tbody, comment));
		});

		xhr.open("GET", "comments.php", true);
		xhr.send();
		human_verif();
	}

	update_comments();

	const AUTHOR_KEY = "aaaa";
	const COMMENT_KEY = "cccc";
	const VERIF_KEYS = ["vvvv1", "vvvv2", "vvvvr"];

	const author = document.getElementById("author");
	const comment = document.getElementById("comment");
	const submit = document.getElementById("submit");
	const sum = document.getElementById("verif");
	submit.addEventListener("click", e => {
		e.preventDefault();
		const req = new XMLHttpRequest();
		const fd = new FormData();

		fd.append(AUTHOR_KEY, author.value);
		fd.append(COMMENT_KEY, comment.value);

		const nums = [verif['num1'], verif['num2'], sum.value];
		for (let i = 0; i < VERIF_KEYS.length; ++i) {
			fd.append(VERIF_KEYS[i], nums[i]);
		}

		req.addEventListener("load", () => {
			const resp = JSON.parse(req.responseText);
			console.log("Response:");
			console.log(resp);
			update_comments();
		});
		req.addEventListener("error", () => {
			console.log("Something went wrong.");
			human_verif();
		});

		req.open("POST", "add.php", true);
		req.send(fd);
	});
});
