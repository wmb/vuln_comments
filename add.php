<?php

/* How to exploit this vulnerability (SQL injection):
 * curl -F 'aaaa=some author' -F 'cccc=some comment' \
 * -F 'vvvv1=10 UNION SELECT * FROM "passwords"' -F 'vvvv2=10' -F 'vvvvr=20' \
 * $URL_OF_THIS_APPLICATION
 */

require_once 'db.php';

$author_key = 'aaaa';
$comment_key = 'cccc';
$num1_key = 'vvvv1';
$num2_key = 'vvvv2';
$sum_key = 'vvvvr';

$success = TRUE;
$response = array();

if (isset($_POST[$author_key])
	&& !empty($_POST[$comment_key])
	&& !empty($_POST[$num1_key])
	&& !empty($_POST[$num2_key])
	&& !empty($_POST[$sum_key])
) {
	$author = empty($_POST[$author_key]) ? NULL : $_POST[$author_key];
	$comment = $_POST[$comment_key];

	$response['comment'] = $comment;
	$response['author'] = $author;

	$nums = array($_POST[$num1_key], $_POST[$num2_key], $_POST[$sum_key]);

	//foreach ($nums as $num) {
		//if (!is_numeric($num)) {
			//$success = FALSE;
		//}
	//}

	$response['nums'] = $nums;

	$n1 = /*(int)*/ $nums[0];
	$n2 = (int) $nums[1];
	$sum = (int) $nums[2];

	//if ((int) $nums[0] + (int) $nums[1] !== (int) $nums[2])
		//$success = FALSE;

	$select_stmt = <<<"EOF"
		SELECT 1, 1
		WHERE CAST ($sum AS INTEGER) - CAST ($n2 AS INTEGER) = $n1
EOF;
	$verif = $dbh->query($select_stmt);

	$success = FALSE;
	if ($verif !== FALSE) {
		$response['verif'] = array();
		while ($row = $verif->fetchArray(SQLITE3_NUM)) {
			$response['verif'][] = $row;
			$success = TRUE;
		}
	}

	if ($success) {
		$insert_stmt =
			'INSERT INTO "comments" ('
			. (isset($author) ? '"author", ' : '')
			. '"comment") VALUES ('
			. (isset($author) ? '?, ' : '')
			. '?)';
		$stmt = $dbh->prepare($insert_stmt);
		$pnum = 0;
		if (!is_null($author))
			$stmt->bindValue(++$pnum, $author, SQLITE3_TEXT);
		$stmt->bindValue(++$pnum, $comment, SQLITE3_TEXT);
		$stmt->execute()->finalize();
	}
}

$dbh->close();

header('Content-Type: application/json; charset=UTF-8');
header('DNT: 1');
header('X-Powered-By: Nothing', TRUE);

echo json_encode(array("success" => $success, "resp" => $response));

ob_end_flush();
